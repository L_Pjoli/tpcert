package com.tpCert.bean;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import com.tpCert.enums.Statut;

@MappedSuperclass
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@PrimaryKeyJoinColumn(name="user_id")
public abstract class Utilisateur {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	protected Long id;
	
	@Column(name="nom_utilisateur", nullable=false)
	protected String nomUtilisateur;
	
	@Column(name="mot_de_passe", nullable=false)
	protected String mdp;
	
	@Column(name="role_user", nullable=false)
	protected String role;
	
	@Column(name="prenom", nullable=true)
	protected String prenom;
	
	@Column(name="nom", nullable=true)
	protected String nom;
	
	@Transient
    private List<String> info;

    public Utilisateur(String nom, String mdp, String role) {
        this.nomUtilisateur = nom;
        this.mdp = mdp;
        this.role = role;
        info = new ArrayList<>();
    }
    
    //Constructeur complet
    public Utilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.mdp = mdp;
        this.role = role;
        info = new ArrayList<>();
    }

    public Utilisateur() {
    	super();
	}

	public String getRole() {
        return role;
    }

    public List<String> fetchInfos() {
        info.add(nom);
        info.add(prenom);
        info.add(nomUtilisateur);
        info.add(role);
        return info;
    }

    public String getNom() {
        return nomUtilisateur;
    }

    public boolean login(String util, String motdp) {
        if (nomUtilisateur.equalsIgnoreCase(util) && mdp.equals(motdp)) {
            return true;
        } else {
            return false;
        }

    }

    public String getNomUtil() {
        return nomUtilisateur;
    }
    
    

    public Long getId() {
		return id;
	}

	public abstract List<Requete> getListStatut(Statut statut);

    public abstract List<Requete> getListeRequetes();

    public abstract void ajoutRequete(Requete nouvelle);

    public abstract List<Requete> getListPerso();
}