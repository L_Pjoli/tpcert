package com.tpCert.bean;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="commentaire")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@PrimaryKeyJoinColumn(name="id")
public class Commentaire {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long comment_id;
	
	@Column(name="commentaire", nullable=false)
	protected String commentaire;
	
	@Transient
    protected Utilisateur auteur;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="client_id", referencedColumnName="user_id", nullable=true)
    protected Client client;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="tech_id", referencedColumnName="user_id", nullable=true)
    protected Technicien technicien;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="requete_id")
	protected Requete requete;
    
	
    Commentaire(String commentaire, Utilisateur auteur) {
        this.commentaire = commentaire;
        if(auteur instanceof Client) {
        	this.client = (Client) auteur;
        }else{
        	this.technicien = (Technicien) auteur;
        }
        this.auteur = auteur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public String getComment() {
        return commentaire;
    }

    public String toString() {
        return getAuteur().getNomUtil() + ": " + getComment() + "\n";
    }
}