package com.tpCert.bean;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tpCert.enums.Statut;

@Entity
@Table(name="client")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@PrimaryKeyJoinColumn(name="user_id")
public class Client extends Utilisateur {
	
	@JsonIgnore
	@ElementCollection
	@OneToMany(mappedBy="client", fetch = FetchType.EAGER)
    List<Requete> listeRequetesClient = new ArrayList<Requete>();

	public Client() {
		super();
	}
	
    public Client(String nomUtilisateur, String mdp) {
        super(nomUtilisateur, mdp, "client");
    }

    public Client(String prenom, String nom, String nomUtilisateur, String mdp) {
        super(prenom, nom, nomUtilisateur, mdp, "client");
    }

    @Override
    public String getRole() {
        return "client";
    }

    @Override
    public void ajoutRequete(Requete nouvelle) {
        listeRequetesClient.add(nouvelle);
    }
    
    @Override
    public List<Requete> getListeRequetes() {
        return listeRequetesClient;
    }

    @Override
    public List<Requete> getListPerso() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Requete> getListStatut(Statut statut) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

	@Override
	public String toString() {
		return "Client [listeRequetesClient=" + listeRequetesClient + ", id=" + id + ", nomUtilisateur="
				+ nomUtilisateur + ", mdp=" + mdp + ", role=" + role + ", prenom=" + prenom + ", nom=" + nom + "]";
	}
    
    
}