package com.tpCert.bean;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tpCert.enums.Categorie;
import com.tpCert.enums.Statut;

@Entity
@Table(name="requete")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@PrimaryKeyJoinColumn(name="requete_id")
public class Requete {    
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long requete_id;
    
    @Column(name="sujet")
    private String sujet = "";
    
    @Column(name="description")
    private String description = "";
    
    @Lob
    @Column(name="fichier", columnDefinition="BLOB")
    private File fichier;

    @Transient
    private Utilisateur utilisateur;
    
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="technicien_id", referencedColumnName="user_id", nullable=true)
    private Technicien technicien;
    
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name="client_id", referencedColumnName="user_id", nullable=true)
    private Client client;
    
    @Column(name="statut")
    private Statut statut;
    
    @Column(name="categorie")
    private Categorie cat;
    
    @JsonIgnore
    @ElementCollection
    @OneToMany(mappedBy="requete", fetch=FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Commentaire> listeCommentaires = new ArrayList<Commentaire>();
    
    @Transient
    private Utilisateur tempo;

    public Requete() {
		super();
	}

    public Requete(String sujet, String description, Client client, Categorie cat){
        this.sujet = sujet;
        this.description = description;
        this.client = client;
        
        statut = Statut.ouvert;
        this.cat = cat;
    }

    public Requete(String sujet, String descrip, Utilisateur utilisateur, Categorie categorie){
        this.utilisateur = utilisateur;
        this.sujet = sujet;
        this.description = descrip;
        this.tempo = client;
        
        if(tempo instanceof Technicien) {
        	technicien = (Technicien) tempo;
        	tempo = null;
        }else {
        	client = (Client) tempo;
        	tempo = null;
        }
        statut = Statut.ouvert;
        cat = categorie;
    }

    public String getSujet() {
        return sujet;
    }
    public void setSujet(String nouveau) {
        sujet = nouveau;
    }
    
    public void setDescription(String nouvelle) {
        description = nouvelle;
    }
    
    public void setCategorie(Categorie choix) {
        cat = choix;
    }

    public File getFichier() {
        return fichier;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    public void finaliser(Statut fin) {
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            this.setStatut(fin);
            this.technicien.ajoutListRequetesFinies(this);
            
            session.update(this);
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void addCommentaire(String comment, Utilisateur user) {
        Commentaire suivant = new Commentaire(comment, user);
    	
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.save(suivant);
            
            listeCommentaires.add(suivant);
            session.update(this);
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    
    public void setTech(Utilisateur tech) {
        if (tech != null) {
            if (tech.getRole().equals("technicien")) {
                this.technicien = (Technicien) tech;
                technicien.ajouterRequeteAssignee(this);
            }
        }
    }
    
    public void setFile(File file) {
        this.fichier = file;
    }
    

    public Statut getStatut() {
        return statut;
    }

    public List<Commentaire> getComments() {
        return listeCommentaires;
    }

    public Categorie getCategorie() {
        return cat;
    }

    public Technicien getTech() {
        return technicien;
    }

    public Utilisateur getClient() {
        return client;
    }

    public String getDescrip() {
        return description;
    }

	public Long getRequete_id() {
		return requete_id;
	}
    
    
}