package com.tpCert.bean;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tpCert.enums.Statut;


@Entity
@Table(name="technicien")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@PrimaryKeyJoinColumn(name="user_id")
public class Technicien extends Utilisateur {
	
	@JsonIgnore
	@ElementCollection
	@OneToMany(mappedBy="technicien", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Requete> ListeRequetesTech;
	
	@JsonIgnore
	@ElementCollection
	@OneToMany(mappedBy="technicien", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Requete> ListeRequetesFinies;
	
	@JsonIgnore
	@ElementCollection
	@OneToMany(mappedBy="technicien", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Requete> ListeRequetesEnCours;
	
	@JsonIgnore
	@ElementCollection
	@OneToMany(mappedBy="technicien", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Requete> ListeRequetesPerso;
	
	public Technicien() {
		super();
	}

    Technicien(String nom, String mdp, String role) {
        super(nom, mdp, "technicien");
        ListeRequetesTech = new ArrayList<Requete>();
        ListeRequetesEnCours = new ArrayList<Requete>();
        ListeRequetesFinies = new ArrayList<Requete>();
        ListeRequetesPerso = new ArrayList<Requete>();
    }

    public Technicien(String prenom, String nom, String nomUtilisateur, String mdp) {
        super(prenom, nom, nomUtilisateur, mdp, "technicien");
        ListeRequetesTech = new ArrayList<Requete>();
        ListeRequetesEnCours = new ArrayList<Requete>();
        ListeRequetesFinies = new ArrayList<Requete>();
        ListeRequetesPerso = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return "technicien";
    }

    @Override
    public List<Requete> getListeRequetes() {
        return ListeRequetesTech;
    }

    public void ajouterRequeteAssignee(Requete assignee) {
      	Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            
            ListeRequetesEnCours.add(assignee);
            ListeRequetesTech.add(assignee);
            
            session.update(assignee);
            session.update(this);
            
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                //transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void ajoutListRequetesFinies(Requete finie) {
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            
            ListeRequetesFinies.add(finie);
            ListeRequetesEnCours.remove(finie);
            session.update(this);
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public void ajoutRequete(Requete nouvelle) {
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            
            ListeRequetesTech.add(nouvelle);
            ListeRequetesPerso.add(nouvelle);
            session.update(this);
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public List<Requete> getListPerso() {
        return ListeRequetesPerso;
    }

    @Override
    public List<Requete> getListStatut(Statut statut) {
        List<Requete> r = new ArrayList<Requete>();
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(statut)) {
                r.add(ListeRequetesTech.get(i));
            }

        }
        return r;
    }

    public String getRequeteParStatut() {
        String ouvert = "Statut ouvert: ";
        int ouv = 0;
        String enTraitement = "Statut En traitement: ";
        int tr = 0;
        String succes = "Statut succ�s: ";
        int su = 0;
        String abandon = "Statut abandonn�e: ";
        int ab = 0;
        String parStatut = "";
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(Statut.ouvert)) {
                ouv++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Statut.enTraitement)) {
                tr++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Statut.finalSucces)) {
                su++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Statut.finalAbandon)) {
                ab++;
            }
        }

        parStatut = ouvert + ouv + "\n"
                + enTraitement + tr + "\n"
                + succes + su + "\n"
                + abandon + ab + "\n";
        return parStatut;

    }

	@Override
	public String toString() {
		return "Technicien [ListeRequetesTech=" + ListeRequetesTech + ", ListeRequetesFinies=" + ListeRequetesFinies
				+ ", ListeRequetesEnCours=" + ListeRequetesEnCours + ", ListeRequetesPerso=" + ListeRequetesPerso
				+ ", id=" + id + ", nomUtilisateur=" + nomUtilisateur + ", mdp=" + mdp + ", role=" + role + ", prenom="
				+ prenom + ", nom=" + nom + "]";
	}
    
    
}