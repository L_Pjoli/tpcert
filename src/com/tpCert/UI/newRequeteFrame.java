package com.tpCert.UI;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.tpCert.Service.BanqueRequetes;
import com.tpCert.bean.Client;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Utilisateur;
import com.tpCert.enums.Categorie;

public class newRequeteFrame extends javax.swing.JFrame {

    Utilisateur utilisateur;
    JFrame prec;
    String path;
    File file;

    public newRequeteFrame(Utilisateur user, JFrame pagePrecedente) {
        initComponents();
        this.setVisible(true);
        this.utilisateur = user;
        prec = pagePrecedente;

    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        titleLbl = new javax.swing.JLabel();
        sujetLbl = new javax.swing.JLabel();
        sujetFld = new javax.swing.JTextField();
        descripLbl = new javax.swing.JLabel();
        descpScroll = new javax.swing.JScrollPane();
        descpArea = new javax.swing.JTextArea();
        fichierLbl = new javax.swing.JLabel();
        pathFld = new javax.swing.JTextField();
        selPathBtn = new javax.swing.JButton();
        catBox = new javax.swing.JComboBox();
        catLbl = new javax.swing.JLabel();
        doneBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        
        setFileChooser();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        titleLbl.setText("Nouvelle requ�te");
        sujetLbl.setText("Sujet de la requ�te:");
        setActionListenerSujet();
        
        setDescriptionArea();
        
        fichierLbl.setText("Fichier:");
        setActionListenerPath();
        setUploadBtn();
        
        setCatBox();
        catLbl.setText("Cat�gorie:");
        
        setBtnTerminer();
        setBtnRevenir();
       
        setAlignment();

        pack();
    }

    
    private void pathFldActionPerformed(java.awt.event.ActionEvent evt) {
        pathFld.setText(fileChooser.getSelectedFile().getPath());
    }
    
    private void selPathBtnActionPerformed(java.awt.event.ActionEvent evt) {
        fileChooser.showOpenDialog(prec);

        pathFldActionPerformed(evt);
        
        file = fileChooser.getSelectedFile();
    }
    
    private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {}

    private void sujetFldActionPerformed(java.awt.event.ActionEvent evt) {}
    
    private void doneBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doneBtnActionPerformed
        try {
            BanqueRequetes.getInstance().nouvelleRequete(sujetFld.getText(), descpArea.getText().replaceAll("\n", " "),
                    utilisateur, Categorie.fromString((String) catBox.getSelectedItem()));

            BanqueRequetes.getInstance().returnDernier().setFile(file);
            
            this.setVisible(false);
            if (utilisateur.getRole().equals("client")) {
                ClientLoggedFrame retourPageClient = new ClientLoggedFrame(utilisateur);
                retourPageClient.setVisible(true);
            } else {
                TechLoggedFrame retourPageTech = new TechLoggedFrame(utilisateur);
                retourPageTech.setVisible(true);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        prec.setVisible(true);

    }

    private void fileChooserFocusGained(java.awt.event.FocusEvent evt) {}
    
    
    
    public void setFileChooser() {
        fileChooser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                fileChooserFocusGained(evt);
            }
        });
    }
    
    public void setActionListenerSujet() {
        sujetFld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sujetFldActionPerformed(evt);
            }
        });
    }
    
   public void setDescriptionArea() {
       descripLbl.setText("Description:");

       descpArea.setColumns(20);
       descpArea.setRows(5);
       descpArea.setBorder(null);
       descpScroll.setViewportView(descpArea);
   }
   
   public void setActionListenerPath() {
       pathFld.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               pathFldActionPerformed(evt);
           }
       });
   }
   
   public void setUploadBtn() {
       selPathBtn.setText("T�l�verser un fichier");
       selPathBtn.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               selPathBtnActionPerformed(evt);
           }
       });
   }
    
   public void setCatBox() {
       catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
       catBox.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               catBoxActionPerformed(evt);
           }
       });
   }
   
   public void setBtnTerminer() {
       doneBtn.setText("Terminer");
       doneBtn.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               doneBtnActionPerformed(evt);
           }
       });
   }
   
   public void setBtnRevenir() {
       quitBtn.setText("Revenir");
       quitBtn.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
               quitBtnActionPerformed(evt);
           }
       });
   }
   
   public void setAlignment() {
       javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
       getContentPane().setLayout(layout);
       layout.setHorizontalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(layout.createSequentialGroup()
               .addContainerGap()
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addGroup(layout.createSequentialGroup()
                       .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                           .addComponent(titleLbl)
                           .addGroup(layout.createSequentialGroup()
                               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                   .addComponent(sujetLbl)
                                   .addComponent(descripLbl)
                                   .addComponent(fichierLbl))
                               .addGap(18, 18, 18)
                               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                   .addComponent(pathFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                   .addComponent(sujetFld, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                   .addComponent(descpScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                                   .addComponent(selPathBtn))))
                       .addContainerGap(30, Short.MAX_VALUE))
                   .addGroup(layout.createSequentialGroup()
                       .addComponent(catLbl)
                       .addGap(63, 63, 63)
                       .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                       .addContainerGap(176, Short.MAX_VALUE))
                   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                       .addGap(124, 124, 124)
                       .addComponent(doneBtn)
                       .addGap(18, 18, 18)
                       .addComponent(quitBtn)
                       .addContainerGap(104, Short.MAX_VALUE))))
           .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
       );
       
       layout.setVerticalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(layout.createSequentialGroup()
               .addContainerGap()
               .addComponent(titleLbl)
               .addGap(18, 18, 18)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                   .addComponent(sujetLbl)
                   .addComponent(sujetFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                   .addComponent(descripLbl)
                   .addComponent(descpScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                   .addComponent(pathFld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                   .addComponent(fichierLbl))
               .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
               .addComponent(selPathBtn)
               .addGap(18, 18, 18)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                   .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                   .addComponent(catLbl))
               .addGap(27, 27, 27)
               .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                   .addComponent(quitBtn)
                   .addComponent(doneBtn))
               .addContainerGap(31, Short.MAX_VALUE))
       );
   }
    

    private javax.swing.JComboBox catBox;
    private javax.swing.JLabel catLbl;
    private javax.swing.JTextArea descpArea;
    private javax.swing.JScrollPane descpScroll;
    private javax.swing.JLabel descripLbl;
    private javax.swing.JButton doneBtn;
    private javax.swing.JLabel fichierLbl;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField pathFld;
    private javax.swing.JButton quitBtn;
    private javax.swing.JButton selPathBtn;
    private javax.swing.JTextField sujetFld;
    private javax.swing.JLabel sujetLbl;
    private javax.swing.JLabel titleLbl;
}
