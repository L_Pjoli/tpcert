package com.tpCert.UI;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.tpCert.Service.BanqueRequetes;
import com.tpCert.Service.BanqueUtilisateurs;
import com.tpCert.bean.Client;
import com.tpCert.bean.Commentaire;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.bean.Utilisateur;
import com.tpCert.enums.Categorie;
import com.tpCert.enums.Statut;

public class TechLoggedFrame extends javax.swing.JFrame {

	Utilisateur utilisateur;
    List<Requete> listTechAssigne;
    List<Requete> listDispo;
    List<Technicien> listTechniciens;
    List<Requete> listRequeteTech;
    Requete requete;
    JFrame prec;
    private File file;
    private boolean showComments = false;
    DefaultListModel d = new DefaultListModel();
    DefaultListModel a = new DefaultListModel();
    DefaultListModel b = new DefaultListModel();
    DefaultListModel c = new DefaultListModel();

    public TechLoggedFrame(Utilisateur tech) throws FileNotFoundException, IOException {
        initComponents();
        this.setVisible(true);
        utilisateur = tech;

        
        listTechAssigne = tech.getListeRequetes();
        listDispo = BanqueRequetes.getInstance().getListRequetes(Statut.ouvert);
        listTechniciens = BanqueUtilisateurs.getUserInstance().getListUtilisateurs("technicien");
        listRequeteTech = tech.getListPerso();

        setListeTechAssignee();
        setListDispo();
        setlistTechniciens();

    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        reqAssignedLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        reqAssignee = new javax.swing.JList();
        reqSelLbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        comLbl = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentsArea = new javax.swing.JTextArea();
        addComLbl = new javax.swing.JLabel();
        comIn = new javax.swing.JTextField();
        catBox = new javax.swing.JComboBox();
        reqDispoLbl = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        reqDispo = new javax.swing.JList();
        prendrereqBtn = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        descReqArea = new javax.swing.JTextArea();
        newRequeteBtn = new javax.swing.JButton();
        assignerReqBtn = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        supportList = new javax.swing.JList();
        finBox = new javax.swing.JComboBox();
        ajoutfichierBtn = new javax.swing.JButton();
        affRapportBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        fileLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        reqAssignedLbl.setText("Les requ�tes qui vous sont assign�es:");
        setRequeteAssignerListSelection();
        jScrollPane1.setViewportView(reqAssignee);

        reqSelLbl.setText("Requ�te s�lectionn�e:");
        setRequeteArea();
        jScrollPane2.setViewportView(requeteArea);

        comLbl.setText("Commentaires:");
        setCommentArea();

        addComLbl.setText("Ajouter commentaire:");
        setComIn();
        setCatBox();

        reqDispoLbl.setText("Requ�tes disponibles non-assign�e:");
        setReqDispoListSelection();
        
        jScrollPane4.setViewportView(reqDispo);

        setPrendreReqBtn();
        setDescReqArea();
        setNewReqBtn();
        setAssignerReqBtn();

        jScrollPane6.setViewportView(supportList);

        setFinBox();
        setAjoutFichierBtn();
        setAffRapportBtn();
        setQuitBtn();
        setFileLbl();

        setAlignment();

        pack();
    }
    
    private void updateRequeteA() {
        if (reqAssignee.getSelectedIndex() >= 0) {
            requete = listTechAssigne.get(reqAssignee.getSelectedIndex());
            requeteArea.setText("Sujet: " + requete.getSujet()
                    + "\nDescription: " + requete.getDescrip()
                    + "\nCat�gorie: " + requete.getCategorie().toString()
                    + "\nStatut: " + requete.getStatut().toString());
            String s = "";
            for (Commentaire cmt : requete.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            if (requete.getFichier() != null && requete.getFichier().exists()) {
                fileLbl.setVisible(true);
                fileLbl.setText("Afficher " + requete.getFichier().getPath());
            } else {
                fileLbl.setVisible(false);
            }
        } else {
            requeteArea.setText("");
        }
        if (requete.getStatut() == Statut.enTraitement) {
            finBox.setEnabled(true);
        } else {
            finBox.setEnabled(false);
        }
    }

    private void updateRequeteD() {
        if (reqDispo.getSelectedIndex() >= 0) {
            requete = listDispo.get(reqDispo.getSelectedIndex());
            descReqArea.setText("Sujet: " + requete.getSujet()
                    + "\nDescription: " + requete.getDescrip()
                    + "\nCat�gorie: " + requete.getCategorie().toString()
                    + "\nStatut: " + requete.getStatut().toString());
        } else {
            descReqArea.setText("");
        }
    }

    private void updateCommentaires() {
        if (requete != null) {
            if (!showComments) {
                String s = "";
                for (Commentaire c : requete.getComments()) {
                    s += c.getAuteur().getNomUtil() + ": " + c.getComment() + "\n";

                }
                commentsArea.setText(s);
                showComments = true;

            }

        }
    }

    private void updateTables() {
        reqAssignee.setModel(a);
        if (!b.isEmpty()) {
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(true);
        } else {
            b.addElement("Il n'y a pas de requ�tes pour le moment.");
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(false);
        }
    }
    
    private void prendrereqBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if (reqDispo.getSelectedIndex() >= 0) {
            Requete r = listDispo.get(reqDispo.getSelectedIndex());
            r.setTech(utilisateur);
            r.setStatut(Statut.enTraitement);
            
          	Transaction transaction = null;
            SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();
                
                session.update(requete);
                
                session.flush();
                transaction.commit();
                session.close();
            } catch (Exception e) {
                if (transaction != null) {
                    //transaction.rollback();
                }
                e.printStackTrace();
            }

            listTechAssigne = utilisateur.getListeRequetes();

            a = new DefaultListModel();
            for (int i = 0; i < listTechAssigne.size(); i++) {
                a.addElement(listTechAssigne.get(i).getSujet());
            }
            reqAssignee.setModel(a);


            b.remove(reqDispo.getSelectedIndex());
            listDispo.remove(r);
        }
        updateTables();

    }
    
    
    private void ajoutfichierBtnActionPerformed(java.awt.event.ActionEvent evt) {
        fileChooser.showOpenDialog(prec);

        File f = fileChooser.getSelectedFile();
        
        if(f != null) {
        	Transaction transaction = null;
            SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();
                
                Requete tempReq = (Requete)session.get(Requete.class, listTechAssigne.get(reqAssignee.getSelectedIndex()).getRequete_id());
                
                tempReq.setFile(f);
                session.update(tempReq);
                
                Requete requeteTest = (Requete)session.get(Requete.class, listTechAssigne.get(reqAssignee.getSelectedIndex()).getRequete_id());
                
                if(requeteTest.getFichier() != null) {
                    fichierAddOkFrame ok = new fichierAddOkFrame();
                    ok.setVisible(true);
                }
                session.flush();
                transaction.commit();
                session.close();
            } catch (Exception e) {
                if (transaction != null) {
                    //transaction.rollback();
                }
                e.printStackTrace();
            }
        }
        updateRequeteA();
    }
    
    private void assignerReqBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if (reqDispo.getSelectedIndex() >= 0) {

        	Transaction transaction = null;
            SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();
                
                Requete r = (Requete)session.get(Requete.class, listDispo.get(reqDispo.getSelectedIndex()).getRequete_id());
                Technicien techChangeStatus = (Technicien) session.get(Technicien.class, listTechniciens.get(supportList.getSelectedIndex()).getId());                
                session.close();
                
                r.setTech(techChangeStatus);
                r.setStatut(Statut.enTraitement);
                
                Session session2 = sessionFactory.openSession();
                transaction = session2.beginTransaction();
                
                b.remove(reqDispo.getSelectedIndex());
                listDispo.remove(r);
                
                session2.update(r);
                session2.update(techChangeStatus);
                
                session2.flush();
                transaction.commit();
                session2.close();
            } catch (Exception e) {
                if (transaction != null) {
                    //transaction.rollback();
                }
                e.printStackTrace();
            }
            updateTables();
        }
    }

    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }

    private void requeteAreaPropertyChange(java.beans.PropertyChangeEvent evt) {}


    private void reqAssigneeValueChanged(javax.swing.event.ListSelectionEvent evt) {
        updateRequeteA();
        updateCommentaires();
    }
    
    private void reqDispoValueChanged(javax.swing.event.ListSelectionEvent evt) {
        if (!listDispo.isEmpty()) {
            updateRequeteD();
        }
    }

    private void catBoxItemStateChanged(java.awt.event.ItemEvent evt) {
        if (catBox.getSelectedIndex() > 0) {
            listTechAssigne.get(reqAssignee.getSelectedIndex()).setCategorie(Categorie.fromString((String) catBox.getSelectedItem()));
            updateRequeteA();
        }
    }

    private void comInActionPerformed(java.awt.event.ActionEvent evt) {}

    
    private void comInKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            requete.addCommentaire(comIn.getText(), utilisateur);
            String s = "";
            for (Commentaire cmt : requete.getComments()) {
                s += cmt.toString();
            }
            commentsArea.setText(s);
            comIn.setText("");
        }
    }
    
    private void fileLblMouseClicked(java.awt.event.MouseEvent evt) {
        try {
            Requete req = listTechAssigne.get(reqAssignee.getSelectedIndex());
            java.awt.Desktop dt = java.awt.Desktop.getDesktop();
            dt.open(req.getFichier());
        } catch (IOException ex) {
            Logger.getLogger(TechLoggedFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void finBoxActionPerformed(java.awt.event.ActionEvent evt) {}

    private void catBoxActionPerformed(java.awt.event.ActionEvent evt) {}

    private void finBoxItemStateChanged(java.awt.event.ItemEvent evt) {
        if (finBox.getSelectedIndex() > 0) {
            listTechAssigne.get(reqAssignee.getSelectedIndex()).finaliser(Statut.fromString((String) finBox.getSelectedItem()));
            updateRequeteA();
        }
    }
    
 

    private void newRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        newRequeteFrame nouvelleRequete = new newRequeteFrame(utilisateur, this);

    }
    
    private void affRapportBtnActionPerformed(java.awt.event.ActionEvent evt) {

        String rap = "";
        Technicien tempo;
        for (int i = 0; i < listTechniciens.size(); i++) {
            rap += listTechniciens.get(i).getNom() + ":\n";
            tempo = listTechniciens.get(i);
            rap += tempo.getRequeteParStatut() + "\n";

        }
        rapportFrame rapport = new rapportFrame(rap);
        rapport.setVisible(true);

    }
    
    private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {}
    
    
    public void setListeTechAssignee() {
        if (listTechAssigne.isEmpty()) {
            a.addElement("Vous n'avez pas encore de requ�te.");
            reqAssignee.setModel(a);
        } else {
            for (int i = 0; i < listTechAssigne.size(); i++) {
                a.addElement(listTechAssigne.get(i).getSujet());
            }
            reqAssignee.setModel(a);
        }
    }
    
    public void setListDispo() {
        if (listDispo.isEmpty()) {
            b.addElement("Il n'y a pas de requ�tes pour le moment.");
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(false);
        } else {
            for (int i = 0; i < listDispo.size(); i++) {
                b.addElement(listDispo.get(i).getSujet());
            }
            reqDispo.setModel(b);
            prendrereqBtn.setEnabled(true);
        }
    }
    
    public void setlistTechniciens() {
        if (listTechniciens.isEmpty()) {
            c.addElement("Il n'y a pas de technicien");
            supportList.setModel(c);
        } else {
            for (int i = 0; i < listTechniciens.size(); i++) {
                c.addElement(listTechniciens.get(i).getNomUtil());
            }
            supportList.setModel(c);

        }
    }
    
    
    public void setRequeteAssignerListSelection() {
        reqAssignee.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                reqAssigneeValueChanged(evt);
            }
        });	
    }
    
    public void setRequeteArea(){
        requeteArea.setColumns(20);
        requeteArea.setRows(5);
        requeteArea.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                requeteAreaPropertyChange(evt);
            }
        });	
    }
    
    public void setCommentArea() {
        commentsArea.setColumns(20);
        commentsArea.setRows(5);
        jScrollPane3.setViewportView(commentsArea);
    }
    
    public void setComIn(){
        comIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comInActionPerformed(evt);
            }
        });
        comIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comInKeyPressed(evt);
            }
        });
    }
    
    public void setCatBox() {
        catBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Changer la cat�gorie", "Poste de travail", "Serveur", "Service web", "Compte usager", "Autre" }));
        catBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                catBoxItemStateChanged(evt);
            }
        });
        catBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catBoxActionPerformed(evt);
            }
        });
    }
   
    
    public void setReqDispoListSelection() {
	    reqDispo.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
	        public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
	            reqDispoValueChanged(evt);
	        }
	    });
    }
    
    public void setPrendreReqBtn(){
        prendrereqBtn.setText("Prendre la requ�te s�lectionn�e");
        prendrereqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prendrereqBtnActionPerformed(evt);
            }
        });
    }
    
    public void setDescReqArea(){
        descReqArea.setColumns(20);
        descReqArea.setRows(5);
        jScrollPane5.setViewportView(descReqArea);	
    }
    
    public void setNewReqBtn() {
        newRequeteBtn.setText("Cr�er une nouvelle requ�te");
        newRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newRequeteBtnActionPerformed(evt);
            }
        });	
    }
    
    public void setAssignerReqBtn() {
        assignerReqBtn.setText("Assigner la requ�te � ce membre");
        assignerReqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignerReqBtnActionPerformed(evt);
            }
        });	
    }
    
    public void setFinBox() {
        finBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Finaliser", "Succ�s", "Abandon", " " }));
        finBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                finBoxItemStateChanged(evt);
            }
        });
        finBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finBoxActionPerformed(evt);
            }
        });
    }
    
    public void setAjoutFichierBtn() {      
        ajoutfichierBtn.setText("Ajouter un fichier");
        ajoutfichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutfichierBtnActionPerformed(evt);
            }
        });
    }
    
    public void setAffRapportBtn() {
        affRapportBtn.setText("Afficher le rapport");
        affRapportBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                affRapportBtnActionPerformed(evt);
            }
        });	
    }
    
    public void setQuitBtn() {
        quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });	
    }
    
    public void setFileLbl() {
        fileLbl.setText("(pas de fichier attach�)");
        fileLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fileLblMouseClicked(evt);
            }
        });	
    }
    
    public void setAlignment() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqDispoLbl)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane5)
                                    .addComponent(prendrereqBtn))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, 0, 0, Short.MAX_VALUE)
                                    .addComponent(assignerReqBtn, javax.swing.GroupLayout.Alignment.LEADING))))
                        .addGap(86, 86, 86))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(reqAssignedLbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reqSelLbl)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comLbl)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(addComLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(comIn)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ajoutfichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(fileLbl)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(quitBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(affRapportBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(newRequeteBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(newRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(affRapportBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(quitBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(reqAssignedLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(comLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(addComLbl)
                                        .addComponent(comIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(ajoutfichierBtn)
                                        .addComponent(fileLbl)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(reqSelLbl)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(catBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(finBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(3, 3, 3)))
                .addGap(38, 38, 38)
                .addComponent(reqDispoLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prendrereqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(assignerReqBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }

    
    private javax.swing.JLabel addComLbl;
    private javax.swing.JButton affRapportBtn;
    private javax.swing.JButton ajoutfichierBtn;
    private javax.swing.JButton assignerReqBtn;
    private javax.swing.JComboBox catBox;
    private javax.swing.JTextField comIn;
    private javax.swing.JLabel comLbl;
    private javax.swing.JTextArea commentsArea;
    private javax.swing.JTextArea descReqArea;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel fileLbl;
    private javax.swing.JComboBox finBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JButton newRequeteBtn;
    private javax.swing.JButton prendrereqBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JLabel reqAssignedLbl;
    private javax.swing.JList reqAssignee;
    private javax.swing.JList reqDispo;
    private javax.swing.JLabel reqDispoLbl;
    private javax.swing.JLabel reqSelLbl;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JList supportList;

}
