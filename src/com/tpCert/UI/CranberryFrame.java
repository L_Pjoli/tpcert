package com.tpCert.UI;


public class CranberryFrame extends javax.swing.JFrame {

    public CranberryFrame() {
        initComponents();
        this.setVisible(true);
    }
  
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        demarrageLbl = new javax.swing.JLabel();
        techBtn = new javax.swing.JButton();
        clientBtn = new javax.swing.JButton();
        
        setWindowAttributes();
        setBtnTech();
        setBtnUser();
        setAlignment();
        
        pack();
    }


    private void techBtnMouseClicked(java.awt.event.MouseEvent evt) {
        this.setVisible(false);
        TechLogin technic = new TechLogin();
    }


    private void clientBtnMouseClicked(java.awt.event.MouseEvent evt) {
        this.setVisible(false);
        ClientFrame visuelClient = new ClientFrame();
    }
    
   public void setWindowAttributes() {
       javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
	   jFrame1.getContentPane().setLayout(jFrame1Layout);
       jFrame1Layout.setHorizontalGroup(
           jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGap(0, 400, Short.MAX_VALUE)
       );
       jFrame1Layout.setVerticalGroup(
           jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGap(0, 300, Short.MAX_VALUE)
       );

       setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
       setBackground(new java.awt.Color(255, 255, 255));
       setName("démarrage");

       demarrageLbl.setText("Démarrer l'application en tant que:");
   }
    
   public void setBtnTech() {
       techBtn.setText("Technicien");
       techBtn.addMouseListener(new java.awt.event.MouseAdapter() {
           public void mouseClicked(java.awt.event.MouseEvent evt) {
               techBtnMouseClicked(evt);
           }
       });
   }
    
   public void setBtnUser() {
	    clientBtn.setText("Utilisateur");
	    clientBtn.addMouseListener(new java.awt.event.MouseAdapter() {
	        public void mouseClicked(java.awt.event.MouseEvent evt) {
	            clientBtnMouseClicked(evt);
	        }
	    });
   }
   
   public void setAlignment() {
       javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
       getContentPane().setLayout(layout);
       layout.setHorizontalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(layout.createSequentialGroup()
               .addGap(32, 32, 32)
               .addComponent(demarrageLbl)
               .addGap(35, 35, 35)
               .addComponent(clientBtn)
               .addGap(18, 18, 18)
               .addComponent(techBtn)
               .addContainerGap(42, Short.MAX_VALUE))
       );
       layout.setVerticalGroup(
           layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
           .addGroup(layout.createSequentialGroup()
               .addContainerGap()
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                   .addComponent(demarrageLbl)
                   .addComponent(clientBtn)
                   .addComponent(techBtn))
               .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
       );
   }

    private javax.swing.JButton clientBtn;
    private javax.swing.JLabel demarrageLbl;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JButton techBtn;
}
