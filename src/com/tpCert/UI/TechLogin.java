package com.tpCert.UI;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.tpCert.Service.BanqueUtilisateurs;
import com.tpCert.bean.Utilisateur;


public class TechLogin extends javax.swing.JFrame {

    public TechLogin() {
        initComponents();
        this.setVisible(true);
    }
    
    private String nomUtil;
    private String motdepasse;
    private Utilisateur potentiel;

    
    private void initComponents() {

        nomUtilisateur = new javax.swing.JLabel();
        utilInput = new javax.swing.JTextField();
        mdpLbl = new javax.swing.JLabel();
        mdpInput = new javax.swing.JTextField();
        okBtn = new javax.swing.JButton();
        annulerBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        nomUtilisateur.setText("Nom d'utilisateur:");
        setActionListener();
        mdpLbl.setText("Mot de passe:");
        setBtnOk();
        setBtnAnnuler();
        setAlignment();

        pack();
    }

    private void utilInputActionPerformed(java.awt.event.ActionEvent evt) {}

    private void okBtnMouseClicked(java.awt.event.MouseEvent evt) {
        try {
            nomUtil = utilInput.getText();
            motdepasse = mdpInput.getText();
            
            potentiel = BanqueUtilisateurs.getUserInstance().chercherNomRole(nomUtil);
            if(potentiel == null){
                ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
                this.setVisible(false);
            } 
            
            if(potentiel.login(nomUtil,motdepasse)){
                this.setVisible(false);
                TechLoggedFrame fenetreTech = new TechLoggedFrame(potentiel);
            }
            else{
                ErrorLoginFrame mauvaisLogin = new ErrorLoginFrame();
                this.setVisible(false);
            }
        } catch (IOException ex) {
            Logger.getLogger(TechLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void annulerBtnMouseClicked(java.awt.event.MouseEvent evt) {
        this.setVisible(false);
        CranberryFrame retour = new CranberryFrame();
    }

    
    public void setActionListener() {
        utilInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                utilInputActionPerformed(evt);
            }
        });
    }

    public void setBtnOk() {
        okBtn.setText("OK");
        okBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                okBtnMouseClicked(evt);
            }
        });
    }
    
    public void setBtnAnnuler() {
        annulerBtn.setText("Annuler");
        annulerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                annulerBtnMouseClicked(evt);
            }
        });
    }
    
    public void setAlignment() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nomUtilisateur)
                    .addComponent(mdpLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(okBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(annulerBtn))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(mdpInput)
                        .addComponent(utilInput, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomUtilisateur)
                    .addComponent(utilInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mdpLbl)
                    .addComponent(mdpInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okBtn)
                    .addComponent(annulerBtn))
                .addContainerGap(23, Short.MAX_VALUE))
        );
    }

    private javax.swing.JButton annulerBtn;
    private javax.swing.JTextField mdpInput;
    private javax.swing.JLabel mdpLbl;
    private javax.swing.JLabel nomUtilisateur;
    private javax.swing.JButton okBtn;
    private javax.swing.JTextField utilInput;

}
