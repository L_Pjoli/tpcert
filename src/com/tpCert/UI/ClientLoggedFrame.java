package com.tpCert.UI;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.tpCert.Service.BanqueRequetes;
import com.tpCert.bean.Client;
import com.tpCert.bean.Commentaire;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.bean.Utilisateur;

public class ClientLoggedFrame extends javax.swing.JFrame {

    Utilisateur utilisateur;
    List<Requete> listeRequetes;
    Requete requete;
    JFrame prec;
    private File file;
    private boolean montrerCommentaire = false;

    
    public ClientLoggedFrame(Utilisateur client) {
        initComponents();
        this.setVisible(true);
        
        this.utilisateur = client;
        listeRequetes = utilisateur.getListeRequetes();
        
        DefaultListModel a = new DefaultListModel();

        if (listeRequetes.isEmpty()) {
            a.addElement("Vous n'avez pas encore de requ�tes.");
        } else {
            for (int i = 0; i < listeRequetes.size(); i++) {
                a.addElement(listeRequetes.get(i).getSujet());
            }
        }
        listRequeteList.setModel(a);
        commentArea.setVisible(false);
    }

    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        listRequeteList = new javax.swing.JList();
        requetesLbl = new javax.swing.JLabel();
        addRequeteBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        modifBtn = new javax.swing.JButton();
        fichierBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        voirComsBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        setReqListArea();
        setAddReqBtn();
        setQuitBtn();
        setajouterCommentBtn();
        setAjoutFichierBtn();
        jLabel1.setText("Modifier la requ�te s�lectionn�e:");
        setRequeteParam();
        setVoirCommentBtn();
        setCommentArea();
        
        setAlignment();
        pack();
    }
    
    private void addRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        newRequeteFrame nouvelleRequete = new newRequeteFrame(utilisateur, this);
    }

    private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        updateRequete();
    }


    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }
    
    private void fichierBtnActionPerformed(java.awt.event.ActionEvent evt) {
        fileChooser.showOpenDialog(prec);
        file = fileChooser.getSelectedFile();

        File f = fileChooser.getSelectedFile();
        
        if(f != null) {
        	Transaction transaction = null;
            SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();

                
                Requete tempReq = (Requete)session.get(Requete.class, listeRequetes.get(listRequeteList.getSelectedIndex()).getRequete_id());
                
                tempReq.setFile(f);
                session.update(tempReq);
                
                Requete requeteTest = (Requete)session.get(Requete.class, listeRequetes.get(listRequeteList.getSelectedIndex()).getRequete_id());
                
                if(requeteTest.getFichier() != null) {
                    fichierAddOkFrame ok = new fichierAddOkFrame();
                    ok.setVisible(true);
                }
                
                session.flush();
                transaction.commit();
                session.close();
            } catch (Exception e) {
                if (transaction != null) {
                    //transaction.rollback();
                }
                e.printStackTrace();
            }
        }
    }

    private void modifBtnActionPerformed(java.awt.event.ActionEvent evt) {
        commentArea.setVisible(true);
        commentArea.requestFocus();
    }

    private void voirComsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voirComsBtnActionPerformed
        if (requete != null) {
            if (!montrerCommentaire) {
                String s = "";
                for (Commentaire c : requete.getComments()) {
                    s += c.toString();
                }
                requeteArea.setText(s);
                montrerCommentaire = true;
                voirComsBtn.setText("Voir la requ�te");
                fichierBtn.setEnabled(false);
                modifBtn.setEnabled(true);
            } else {
                updateRequete();
            }
        }
    }
    
    private void updateRequete() {
        requete = listeRequetes.get(listRequeteList.getSelectedIndex());
        requeteArea.setText("Sujet: " + requete.getSujet()
                + "\nDescription: " + requete.getDescrip()
                + "\nCat�gorie: " + requete.getCategorie().toString()
                + "\nStatut: " + requete.getStatut().toString());
        montrerCommentaire = false;
        voirComsBtn.setText("Voir les commentaires");
        fichierBtn.setEnabled(true);
        modifBtn.setEnabled(false);
    }
    
    private void commentAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_commentAreaKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            requete.addCommentaire(commentArea.getText(), utilisateur);
            String s = "";
            for (Commentaire c : requete.getComments()) {
                s += c.toString();
            }
            requeteArea.setText(s);
            commentArea.setVisible(false);
            modifBtn.requestFocus();
        }
    }
    
    public void setReqListArea(){
        listRequeteList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Vous n'avez pas de requ�tes." };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listRequeteList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listRequeteListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(listRequeteList);
        requetesLbl.setText("Vos requ�tes:");
    }
    
    public void setAddReqBtn(){
    	addRequeteBtn.setText("Faire une nouvelle requ�te");
        addRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRequeteBtnActionPerformed(evt);
            }
        });
    }
    
    public void setQuitBtn() {
        quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });
    }
    
    public void setajouterCommentBtn() {

        modifBtn.setText("Ajouter un commentaire");
        modifBtn.setEnabled(false);
        modifBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifBtnActionPerformed(evt);
            }
        });
    }
    
    public void setAjoutFichierBtn() {
        fichierBtn.setText("Ajouter un fichier");
        fichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fichierBtnActionPerformed(evt);
            }
        });

    }
    
    public void setRequeteParam(){
        requeteArea.setColumns(20);
        requeteArea.setEditable(false);
        requeteArea.setLineWrap(true);
        requeteArea.setRows(5);
        jScrollPane2.setViewportView(requeteArea);
    }
    
    public void setVoirCommentBtn(){
        voirComsBtn.setBackground(new java.awt.Color(102, 153, 255));
        voirComsBtn.setText("Voir les commentaires");
        voirComsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voirComsBtnActionPerformed(evt);
            }
        });
    }
    
    public void setCommentArea(){
        commentArea.setColumns(20);
        commentArea.setLineWrap(true);
        commentArea.setRows(5);
        commentArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                commentAreaKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(commentArea);
    }
    
    
    public void setAlignment() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addRequeteBtn)
                    .addComponent(requetesLbl)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(quitBtn))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voirComsBtn)
                            .addComponent(fichierBtn)
                            .addComponent(modifBtn)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(addRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(requetesLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(quitBtn))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(voirComsBtn)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(modifBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)))))
                .addContainerGap())
        );
    }
    

    
    private javax.swing.JButton addRequeteBtn;
    private javax.swing.JTextArea commentArea;
    private javax.swing.JButton fichierBtn;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList listRequeteList;
    private javax.swing.JButton modifBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JLabel requetesLbl;
    private javax.swing.JButton voirComsBtn;
}
