package com.tpCert.enums;

public enum Statut {

    ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succ�s");
    String valeur;

    //Constructeur des statuts
    Statut(String s) {
        this.valeur = s;
    }

    public String getValueString() {
        return valeur;
    }

    //Retourne le statut en fonction d'une string donnée
    public static Statut fromString(String text) {
        if (text != null) {
            if (Statut.ouvert.getValueString().equals(text)) {
                return Statut.ouvert;
            } else if (Statut.enTraitement.getValueString().equals(text)) {
                return Statut.enTraitement;
            } else if (Statut.finalAbandon.getValueString().equals(text)) {
                return Statut.finalAbandon;
            } else if (Statut.finalSucces.getValueString().equals(text)) {
                return Statut.finalSucces;
            }

        }
        return null;
    }
}
