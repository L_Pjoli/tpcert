package com.tpCert.enums;

public enum Categorie {

    posteDeTravail("Poste de travail"), serveur("Serveur"),
    serviceWeb("Service web"), compteUsager("Compte usager"), autre("Autre");
    String value;

    Categorie(String s) {
        this.value = s;
    }

    public String getValueString() {
        return value;
    }

    //Retourne la Catégorie en fonction d'une String donnée
    public static Categorie fromString(String text) {
        if (text != null) {
            if (Categorie.posteDeTravail.getValueString().equals(text)) {
                return Categorie.posteDeTravail;
            } else if (Categorie.serveur.getValueString().equals(text)) {
                return Categorie.serveur;
            } else if (Categorie.serviceWeb.getValueString().equals(text)) {
                return Categorie.serviceWeb;
            } else if (Categorie.compteUsager.getValueString().equals(text)) {
                return Categorie.compteUsager;
            } else if (Categorie.autre.getValueString().equals(text)) {
                return Categorie.autre;
            }
        }
        return null;
    }
}
