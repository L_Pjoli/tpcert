package com.tpCert.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.tpCert.bean.Client;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.bean.Utilisateur;
import com.tpCert.enums.Categorie;
import com.tpCert.enums.Statut;

class TestUtilisateur {
	
	Technicien tech;
	Utilisateur user;
	Requete requete;

	@BeforeEach
	void setUp() throws Exception {
		tech = new Technicien("lp", "jolicoeur", "skigma", "1234");
		user = new Client("LP", "Joli", "skigma", "1234");
		requete = new Requete("help", "a laide", Mockito.mock(Client.class), Categorie.serviceWeb);
	}

	@AfterEach
	void tearDown() throws Exception {
		
		
	}

	@Test
	void testAjoutRequete() {
		tech.ajoutRequete(requete);
		assertTrue(tech.getListeRequetes().size() == 1);
	}
	
	@Test
	void testAjoutListRequeteFini() {
		tech.ajoutRequete(requete);
		tech.ajoutListRequetesFinies(requete);
		String requetes = tech.getRequeteParStatut();
		assertEquals(requetes,  "Statut ouvert: 1\n" + 
								"Statut En traitement: 0\n" + 
								"Statut succ�s: 0\n" + 
								"Statut abandonn�e: 0\n");
	}
	
	@Test
	void testAjoutRequeteAssignee() {
		tech.ajouterRequeteAssignee(requete);
		assertTrue(tech.getListeRequetes().size() == 1);
	}
	
	@Test
	void testGetListStatus() {
		tech.ajoutRequete(requete);
		assertTrue(tech.getListStatut(Statut.ouvert).size() == 1);
	}
	
	@Test
	void testLogin() {
		assertTrue(user.login("skigma", "1234"));
	}

}
