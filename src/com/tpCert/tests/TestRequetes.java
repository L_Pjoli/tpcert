package com.tpCert.tests;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.tpCert.bean.Client;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.enums.Categorie;
import com.tpCert.enums.Statut;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TestRequetes {

	Requete requete;
	
	@BeforeEach
	void setUp() throws Exception{
		requete = new Requete("help", "a l'aide", Mockito.mock(Client.class), Categorie.serveur);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testFinaliserClient(){
		assertThrows(NullPointerException.class, () -> {requete.finaliser(Statut.finalSucces);});
		
	}
	
	@Test
	void testFinaliserSetTech() {
		Technicien tech = Mockito.mock(Technicien.class);
		when(tech.getRole()).thenReturn("technicien");
		requete.setTech(tech);
		requete.finaliser(Statut.finalSucces);
		assertTrue(requete.getStatut() == Statut.finalSucces);
	}
	
	@Test
	void testAddCommentaire() {
		requete.addCommentaire("ohhhh", Mockito.mock(Client.class));
		assertEquals(requete.getComments().get(0).getComment(), "ohhhh");
	}

}
