package com.tpCert.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tpCert.Service.BanqueUtilisateurs;

class TestService {
	
	BanqueUtilisateurs banqueUser;

	@BeforeEach
	void setUp() throws Exception {
		banqueUser = BanqueUtilisateurs.getUserInstance();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAddUtilisateurAndTestGetListUtilisateurs() throws FileNotFoundException {
		System.out.println(banqueUser.getListUtilisateurs("client").size());
		assertTrue(banqueUser.getListUtilisateurs("client").size() == 4);
	}

}
