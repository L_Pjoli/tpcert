package com.tpCert.Service;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.tpCert.bean.Client;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.bean.Utilisateur;
import com.tpCert.enums.Categorie;
import com.tpCert.enums.Statut;

public class BanqueRequetes {

	private static BanqueRequetes instance = null;
	private ArrayList<Requete> listeRequetes;

	private BanqueRequetes() {
		listeRequetes = new ArrayList<Requete>(100);

	}

	public static BanqueRequetes getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new BanqueRequetes();
			instance.chargerRequetes();
			return instance;

		}
	}

	public void nouvelleRequete(String sujet, String desrcrip, Utilisateur client, Categorie cat) {
		Requete nouvelle = new Requete(sujet, desrcrip, client, cat);
		
		Transaction transaction = null;
		SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml")
				.buildSessionFactory();
		try (Session session = sessionFactory.openSession()) {
			transaction = session.beginTransaction();
			
			
    		listeRequetes.add(nouvelle);
    		client.ajoutRequete(nouvelle);
    		
			session.save(nouvelle);
            session.update(client);
            
            Client clientTemp = (Client)session.get(Client.class, client.getId());

			transaction.commit();
			session.close();
		} catch (Exception e) {
			if (transaction != null) {
				//transaction.rollback();
			}
			e.printStackTrace();
		}
		sessionFactory.close();
	}

	public ArrayList getListRequetes(Statut statut) {
		ArrayList<Requete> ceStatut = new ArrayList<Requete>();
		for (int i = 0; i < listeRequetes.size(); i++) {
			if (listeRequetes.get(i).getStatut().equals(statut)) {
				ceStatut.add(listeRequetes.get(i));
			}

		}
		return ceStatut;
	}

	public void assignerRequete(Technicien technicien, Requete selectionee) {
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            
    		selectionee.setStatut(Statut.enTraitement);
    		selectionee.setTech(technicien);
            session.update(this);
            
            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
	}
	

	public Requete returnDernier() {
		return listeRequetes.get(listeRequetes.size() - 1);
	}

	private void chargerRequetes() {
		Transaction transaction = null;
		SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml")
				.buildSessionFactory();
		try (Session session = sessionFactory.openSession()) {
			transaction = session.beginTransaction();

			Query<Requete> query = session.createQuery("from Requete");
			List<Requete> listeRequete = query.list();

			for (Requete requeteI : listeRequete) {
				this.listeRequetes.add(requeteI);
			}

			session.flush();
			transaction.commit();
			session.close();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		sessionFactory.close();
	}
}
