package com.tpCert.Service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.tpCert.bean.Client;
import com.tpCert.bean.Requete;
import com.tpCert.bean.Technicien;
import com.tpCert.enums.Categorie;

public class PopulerBD {
	
	public static void populer() {
		Transaction transaction = null;
	    SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
	    try {
	    	Session session = sessionFactory.openSession();
	        transaction = session.beginTransaction();
	    	
	        
	        Client client = new Client("lp", "joli", "skigma", "1234");
	    	Technicien tech = new Technicien("lp", "joli", "skigmaTech", "1234");
	    	Client client1 = new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool");
	    	Client client2 = new Client("Martine", "Jodoin", "martinejodoin", "moiaussi");
	    	Client client3 = new Client("Pauline", "St-Onge", "popoline", "monchatkiki");
	    	Technicien tech1 = new Technicien("Richard", "Chabot", "chabotr", "chabotte");
	    	Technicien tech2 = new Technicien("Louise", "Boisvert", "techguest", "password");
	    	
	    	Requete requete1 = new Requete("Exemple 1", "test d'une requete", tech2, Categorie.posteDeTravail);
	    	Requete requete2 = new Requete("Exemple 2", "une autre requete", client3, Categorie.serveur);
	    	
	        session.saveOrUpdate(client);
	        session.saveOrUpdate(tech);
	        session.saveOrUpdate(client1);
	        session.saveOrUpdate(client2);
	        session.saveOrUpdate(client3);
	        session.saveOrUpdate(tech1);
	        session.saveOrUpdate(tech2);
	        
	        session.saveOrUpdate(requete1);
	        session.saveOrUpdate(requete2);
	        

	        session.flush();
	        transaction.commit();
	        session.close();
	    } catch (Exception e) {
	        if (transaction != null) {
	            transaction.rollback();
	        }
	        e.printStackTrace();
	    }
	    sessionFactory.close();
	}
}
