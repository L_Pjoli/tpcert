package com.tpCert.Service;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.tpCert.bean.Client;
import com.tpCert.bean.Technicien;
import com.tpCert.bean.Utilisateur;

public class BanqueUtilisateurs {

    private static BanqueUtilisateurs ListeUsers = null;
    private List<Utilisateur> listeUtilisateurs;

    private BanqueUtilisateurs() throws FileNotFoundException {
        listeUtilisateurs = new ArrayList<Utilisateur>(100);

    }

    public void chargerUtilisateur() throws FileNotFoundException {
    	
    	//PopulerBD.populer();
    	
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try {
        	Session session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            
            List<Client> listeClient = session.createQuery("from Client").list();
            List<Technicien> listeTech = session.createQuery("from Technicien").list();
            
            for(Client clientI : listeClient) {
            	this.listeUtilisateurs.add(clientI);
            }
            for(Technicien techI : listeTech) {
            	this.listeUtilisateurs.add(techI);
            }

            session.flush();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        sessionFactory.close();
        
    }

    public static BanqueUtilisateurs getUserInstance() throws FileNotFoundException {
        if (ListeUsers != null) {
            return ListeUsers;
        } else {
            ListeUsers = new BanqueUtilisateurs();
            ListeUsers.chargerUtilisateur();
            return ListeUsers;
        }
    }

    public ArrayList getListUtilisateurs(String role) {
        ArrayList<Utilisateur> users = new ArrayList<Utilisateur>(100);
        for (int i = 0; i < listeUtilisateurs.size(); i++) {
            if (listeUtilisateurs.get(i).getRole().equals(role)) {
                users.add(listeUtilisateurs.get(i));
            }
        }
        return users;
    }

    public Utilisateur chercherUtilisateur(Utilisateur rechercher) {
        if (listeUtilisateurs.contains(rechercher)) {
            return listeUtilisateurs.get(listeUtilisateurs.indexOf(rechercher));
        } else {
            return null;
        }
    }

    public Utilisateur chercherNomRole(String nomVoulu) {
        for (int i = 0; i < listeUtilisateurs.size(); i++) {
            if (listeUtilisateurs.get(i).getNom().equalsIgnoreCase(nomVoulu)) {
                return listeUtilisateurs.get(i);
            }
        }
        return null;
    }

    public void addUtilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        if (role.equals("client")) {
            Client client = new Client(prenom, nom, nomUtilisateur, mdp);
            listeUtilisateurs.add(client);
            addToDB(client);
        } else {
            Technicien tech = new Technicien(prenom, nom, nomUtilisateur, mdp);
            listeUtilisateurs.add(tech);
            addToDB(tech);
        }
    }
    
    public void addToDB(Utilisateur utilisateur) {
        Transaction transaction = null;
        SessionFactory sessionFactory = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            
            session.save(utilisateur);
            
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        sessionFactory.close();
    }
}